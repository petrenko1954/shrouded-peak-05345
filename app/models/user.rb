#Листинг 6.9: Валидация наличия атрибута name. ЗЕЛЕНЫЙ app/models/user.rb
#Листинг 6.12: Валидация наличия атрибута email. ЗЕЛЕНЫЙ app/models/user.rb
#Листинг 6.16: Добавление валидации длины для атрибута name. ЗЕЛЕНЫЙ app/models/user.rb
#Листинг 6.21: Валидация формата email с помощью регулярного выражения. ЗЕЛЕНЫЙ app/models/user.rb
#Листинг 6.24: Валидация уникальности адресов электронной почты. ЗЕЛЕНЫЙ app/models/user.rb
#Листинг 6.26: Валидация уникальности адресов электронной почты, нечувствительная к регистру. ЗЕЛЕНЫЙ app/models/user.rb
#Листинг 6.31: Обеспечение уникальности атрибута email за счет перевода в нижний регистр. ЗЕЛЕНЫЙ app/models/user.rb
#Листинг 6.34: Добавление has_secure_password к модели User. КРАСНЫЙ app/models/user.rb
#Листинг 6.39: Законченная реализация безопасных паролей. ЗЕЛЕНЫЙ app/models/user.rb Листинг 8.31: Добавление метода для генерации токена. app/models/user.rbЛистинг 8.32: Добавление метода remember к модели User. ЗЕЛЕНЫЙ app/models/user.rbЛистинг 8.33: Добавление метода authenticated? к модели User. app/models/user.rbЛистинг 8.38: Добавление метода forget к модели User. app/models/user.rbЛистинг 8.45: Обновление authenticated? для обработки несуществующего дайджеста. ЗЕЛЕНЫЙ app/models/user.rbЛистинг 10.3: Добавление кода активации аккаунта к модели User. ЗЕЛЕНЫЙ app/models/user.rbЛистинг 10.33: Добавление методов активации пользователя в модель User. app/models/user.rbЛистинг 11.10: У пользователя has_many (много) микросообщений. ЗЕЛЕНЫЙ app/models/user.rb Листинг 11.18: Обеспечение того, что микросообщения пользователя удаляются вместе с пользователем.Листинг 11.44: Предварительная реализация потока микросообщений. app/models/user.rb 

class User < ActiveRecord::Base
#has_many :microposts
has_many :microposts, dependent: :destroy
  default_scope -> { order(created_at: :desc) }

  attr_accessor :remember_token, :activation_token, :reset_token
  before_save   :downcase_email
  before_create :create_activation_digest
  validates :name, presence: true, length: { maximum: 50 }

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
  has_secure_password
  validates :password, presence: true, length: { minimum: 6 }, allow_nil: true

  # Возвращает дайджест данной строки
  def User.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  def User.new_token
    SecureRandom.urlsafe_base64
  end

  def remember
    self.remember_token = User.new_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  # Возвращает true, если данный токен совпадает с дайджестом.
  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

  # Забывает пользователя
  def forget
    update_attribute(:remember_digest, nil)
  end

  # Активирует аккаунт.
  def activate
    update_attribute(:activated,    true)
    update_attribute(:activated_at, Time.zone.now)
  end

  # Отправляет электронное письмо для активации.
  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end

  # Устанавливает атрибуты для сброса пароля.
  def create_reset_digest
    self.reset_token = User.new_token
    update_attribute(:reset_digest,  User.digest(reset_token))
    update_attribute(:reset_sent_at, Time.zone.now)
  end

  # Отправляет электронное письмо для сброса пароля.
  def send_password_reset_email
    UserMailer.password_reset(self).deliver_now
  end

  # Возвращает true, если истек срок давности ссылки для сброса пароля .
  def password_reset_expired?
    reset_sent_at < 2.hours.ago
  end
#10.33
 # Активирует аккаунт.
  def activate
    update_attribute(:activated,    true)
    update_attribute(:activated_at, Time.zone.now)
  end

  # Отправляет электронное письмо для активации.
  def send_activation_email
    UserMailer.account_activation(self).deliver_now
  end
# Определяет прото-ленту.
  # Полная реализация в "Следовании за пользователями".
  def feed
    Micropost.where("user_id = ?", id)
  end


                    private

    # Переводит адрес электронной почты в нижний регистр.
    def downcase_email
      self.email = email.downcase
    end

    # Создает и присваивает активационнй токен и дайджест.
    def create_activation_digest
      self.activation_token  = User.new_token
      self.activation_digest = User.digest(activation_token)
    end
end
