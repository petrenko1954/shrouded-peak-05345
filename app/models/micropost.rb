class Micropost < ApplicationRecord
#Листинг 11.4: Валидация user_id для микросообщений. ЗЕЛЕНЫЙ app/models/micropost.rb Листинг 11.9: Микросообщения belongs_to (принадлежат) пользователю.Листинг 11.16: Упорядочивание микросообщений с помощью default_scope.Листинг 11.56: Добавление загрузчика изображений к модели Micropost . app/models/micropost.rb Листинг 11.61: Добавление валидации для изображений. app/models/micropost.rb 
  
belongs_to :user
  default_scope -> { order(created_at: :desc) }
mount_uploader :picture, PictureUploader

validates :user_id, presence: true
  validates :content, presence: true, length: { maximum: 140 }
 validate  :picture_size
                         private

    # Проводит валидацию размера загруженного изображения.
    def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, "should be less than 5MB")
      end
    end
end


