#Листинг 8.12: Метод log_in. app/helpers/sessions_helper.rb Листинг 8.14: Поиск текущего #пользователя в сессии. app/helpers/sessions_helper.rbЛистинг 8.26: Метод log_out. app/helpers/#sessions_helper.rb Листинг 8.35: Запоминание пользователя. app/helpers/sessions_helper.rbЛистинг 8.39: Выход из постоянной сессии. app/helpers/sessions_helper.rbЛистинг 8.57: Удаление вызова исключения. ЗЕЛЕНЫЙ app/helpers/sessions_helper.rbЛистинг 9.24: Метод current_user?. app/helpers/sessions_helper.rb  Листинг 9.27: Код реализации дружелюбной переадресации. app/helpers/sessions_helper.rbЛистинг 10.26: Использование обобщенного метода authenticated? в current_user. app/helpers/sessions_helper.rb     

module SessionsHelper

  # Осуществляет вход данного пользователя.
  def log_in(user)
    session[:user_id] = user.id
  end
# Запоминает пользователя в постоянную сессию.
  def remember(user)
    user.remember
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
  end

# Возвращает true, если заданный пользователь является текущим.
  def current_user?(user)
    user == current_user
  end


# Возвращает пользователя, соответствующего remember-токену в куки.
  def current_user
    if (user_id = session[:user_id])
      @current_user ||= User.find_by(id: user_id)
    elsif (user_id = cookies.signed[:user_id])
      user = User.find_by(id: user_id)
 #10.26 Возвращает текущего пользователя, осуществившего вход (если он есть).
            if user && user.authenticated?(:remember, cookies[:remember_token])

        log_in user
        @current_user = user
      end
    end
  end
# Возвращает true, если пользователь вошел, иначе false.
  def logged_in?
    !current_user.nil?
  end

 # Забывает постоянную сессии.
  def forget(user)
    user.forget
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end
 # Осуществляет выход текущего пользователя.
  def log_out
forget(current_user)
    session.delete(:user_id)
    @current_user = nil
  end
# Перенаправляет к сохраненному расположению (или по умолчанию).
  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end

  # Сохраняет запрошенный URL.
  def store_location
    session[:forwarding_url] = request.url if request.get?
  end
end

