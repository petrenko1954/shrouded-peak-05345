class AccountActivationsController < ApplicationController
#Листинг 10.29: Действие edit для активирования аккаунтов. app/controllers/account_activations_controller.rb Листинг 10.35: Активация аккаунта через объект модели User. app/controllers/account_activations_controller.rb 
  def edit
     user = User.find_by(email: params[:email])
    if user && !user.activated? && user.authenticated?(:activation, params[:id])
      user.activate
      log_in user
      flash[:success] = "Account activated!"
      redirect_to user
    else
      flash[:danger] = "Invalid activation link"
      redirect_to root_url
    end
  end
end
