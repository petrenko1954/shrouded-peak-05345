#Листинг 8.4: Предварительная версия действия Sessions create. app/controllers/sessions_controller.rb Листинг 8.5: Поиск и аутентификация пользователей. app/controllers/sessions_controller.rb Листинг 8.6: (Неудачная) попытка обработки провального входа. app/controllers/sessions_controller.rb Листинг 8.9: Правильный код для провального входа. ЗЕЛЕНЫЙ app/controllers/sessions_controller.rb Листинг 8.27: Удаление сессии (выход пользователя). app/controllers/sessions_controller.rbЛистинг 8.34: Вход и запоминание пользователя. app/controllers/sessions_controller.rbЛистинг 8.42: Выход только тогда, когда пользователь является вошедшим в систему. ЗЕЛЕНЫЙ app/controllers/sessions_controller.rbЛистинг 8.49: Обработка флажка “remember me”. app/controllers/sessions_controller.rb Листинг 9.29: Действие create Sessions-контроллера с дружелюбной переадресацией. app/controllers/sessions_controller.rb 
#Листинг 10.30: Предотвращение входа неактивированных пользователей. app/controllers/sessions_controller.rb     

class SessionsController < ApplicationController

  def new
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])

 if user.activated?
        log_in user
        params[:session][:remember_me] == '1' ? remember(user) : forget(user)
        redirect_back_or user

  else
     message  = "Account not activated. "
        message += "Check your email for the activation link."
        flash[:warning] = message
        redirect_to root_url
    end
else
      flash.now[:danger] = 'Invalid email/password combination'
      render 'new'
    end
  end

  def destroy
log_out if logged_in?

    redirect_to root_url

  end
end

