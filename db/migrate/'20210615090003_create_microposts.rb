class CreateMicroposts < ActiveRecord::Migration[5.0]
  def change
    create_table :microposts do |t|
      t.text :content
      t.references :user, foreign_key: true

      t.timestamps
    end
#Листинг 11.1: Миграция Micropost с добавлением индекса. db/migrate/[timestamp]_create_microposts.rb 
    add_index :microposts, [:user_id, :created_at]

  end
end
